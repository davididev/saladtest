﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Data representation of a vegetable
public class Vegetable 
{
    public int ID = 0;
    public float cuttingTime = 0.2f;

    /// <summary>
    /// Define a vegetable for the Vars Database
    /// </summary>
    /// <param name="i">Vegetable ID</param>
    /// <param name="c">Cutting time (seconds it takes to cut said vegetable)</param>
    public Vegetable(int i, float c)
    {
        ID = i;
        cuttingTime = c;
    }
}
