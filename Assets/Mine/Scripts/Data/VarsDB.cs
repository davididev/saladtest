﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VarsDB
{
    public Vegetable[] vegetables;
    public static VarsDB Instance = new VarsDB();

    /// <summary>
    /// Initilize the data factory
    /// </summary>
    public VarsDB()
    {
        vegetables = new Vegetable[6];
        SetVegetableID(0, 0.4f);
        SetVegetableID(1, 0.45f);
        SetVegetableID(2, 0.48f);
        SetVegetableID(3, 1.00f);
        SetVegetableID(4, 1.05f);
        SetVegetableID(5, 1.15f);
    }

    /// <summary>
    /// Set an entry in the data factory
    /// </summary>
    /// <param name="id">Which entry in the array</param>
    /// <param name="ctime">The time it takes to cut</param>
    public void SetVegetableID(int id, float ctime)
    {
        vegetables[id] = new Vegetable(id, ctime);
    }

    /// <summary>
    /// Obtain the vegetable via ID
    /// </summary>
    /// <param name="id">ID of vegetable</param>
    /// <returns>A Vegetable data object</returns>
    public Vegetable GetVegetable(int id)
    {
        return vegetables[id];
    }
}
