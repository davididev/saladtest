﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Data representation of a salad
public class Salad 
{
    public int[] ingredients;
    public bool[] cut;  //Realized after the design doc this variable would help

    /// <summary>
    /// Create default salad.  To actually set it up, use AddIngredients.
    /// </summary>
    public Salad()
    {
        cut = new bool[] { false, false, false };
        ingredients = new int[3] { -1, -1, -1 };
    }

    /// <summary>
    /// Create a duplicate instead of a reference
    /// </summary>
    /// <param name="clone"></param>
    public Salad(Salad clone)
    {
        this.ingredients = new int[clone.ingredients.Length];
        for(int i = 0; i < this.ingredients.Length; i++)
        {
            this.ingredients[i] = clone.ingredients[i];
        }
        this.cut = new bool[clone.cut.Length];
        for (int i = 0; i < this.cut.Length; i++)
        {
            this.cut[i] = clone.cut[i];
        }

        
    }

    public void AddIngredients(int[] ing)
    {
        int x = 0;
        for(int i = 0; i < ingredients.Length; i++)
        {
            if(ingredients[i] == -1)
            {
                ingredients[i] = ing[x];
                x++;
            }
            if (x >= ing.Length)  //Overflowed the salad- break
                break;
        }
    }

    /// <summary>
    /// Checks to see if this salad matches another salad.  It's a matter of which ones have the same # of ingredients, order doesn't matter.
    /// </summary>
    /// <param name="compare">Salad to compare to</param>
    /// <returns>True if they're equal, false if not</returns>
    public bool Equals(Salad compare)
    {
        int[] veggieTypeCount1 = new int[6] { 0, 0, 0, 0, 0, 0 };
        int[] veggieTypeCount2 = new int[6] { 0, 0, 0, 0, 0, 0 };

        for(int i = 0; i < ingredients.Length; i++)
        {
            int x = ingredients[i];
            if (x > -1)
            {
                veggieTypeCount1[x]++;
            }
        }

        for (int i = 0; i < compare.ingredients.Length; i++)
        {
            int x = compare.ingredients[i];
            if (x > -1)
            {
                veggieTypeCount2[x]++;
            }
        }

        for(int i = 0; i < veggieTypeCount1.Length; i++)
        {
            if (veggieTypeCount1[i] != veggieTypeCount2[i])
                return false;
        }

        return true;
    }

    /// <summary>
    /// Determines if it's all chopped up and ready to go
    /// </summary>
    /// <returns></returns>
    public bool IsReady()
    {
        bool ready = true;
        for(int i = 0; i < ingredients.Length; i++)
        {
            if(ingredients[i] != -1)
            {
                if (cut[i] == false)
                    return false;  //This one hasn't been cut yet- currently not ready
            }
        }

        return ready;
    }

    /// <summary>
    /// Combine all the vegetables in the salad data to get the cut time.  Should only be called once we use the cutting board.
    /// </summary>
    /// <returns></returns>
    public float GetTotalCutTime()
    {
        float t = 0f;
        for (int i = 0; i < ingredients.Length; i++)
        {
            if (ingredients[i] > -1)
            { 
                if(cut[i] == false)
                {
                    cut[i] = true;
                    t += VarsDB.Instance.GetVegetable(ingredients[i]).cuttingTime;
                }
            }
        }
        return t;
    }
}
