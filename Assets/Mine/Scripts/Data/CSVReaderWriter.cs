﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CSVReaderWriter 
{
    private List<string> writtenLines;
    public CSVReaderWriter()
    {
        writtenLines = new List<string>();
        writtenLines.Clear();
    }

    /// <summary>
    /// Obtain all the lines from a file.  You can inspect them individually with GetParameters
    /// </summary>
    /// <param name="fileName">Name of file in the persistent path</param>
    /// <returns></returns>
    public string[] GetLines(string fileName)
    {
        string path = Application.persistentDataPath + "/"+ fileName;
        if (!File.Exists(path))
            return null;
        string contents = File.ReadAllText(path);
        
            return contents.Split('\n');
    }

    /// <summary>
    /// Read a line and return an int or a string
    /// </summary>
    /// <param name="line"></param>
    /// <returns></returns>
    public object[] GetParameters(string line)
    {
        string[] rawObjects = line.Split(',');
        object[] realObjects = new object[rawObjects.Length];

        for(int i = 0; i < rawObjects.Length; i++)
        {
            int possibleInt = 0;
            if(int.TryParse(rawObjects[i], out possibleInt))
            {
                realObjects[i] = possibleInt;  //Assign an int
            }
            else
            {
                realObjects[i] = rawObjects[i];  //Assign a string
            }
        }

        return realObjects;
    }

    /// <summary>
    /// Call this when we start writing the file
    /// </summary>
    public void ResetWriteFile()
    {
        writtenLines.Clear();
    }
    
    /// <summary>
    /// Write to a temporary list, be sure to clear first!
    /// </summary>
    /// <param name="parameters"></param>
    public void WriteLine(object[] parameters)
    {
        string str = "";
        for(int i = 0; i < parameters.Length; i++)
        {
            str = str + parameters[i].ToString();
            if (i < parameters.Length - 1)
                str = str + ',';
        }
        writtenLines.Add(str);
    }


    public void WriteFile(string fileName)
    {
        string path = Application.persistentDataPath + "/" + fileName;
        string contents = "";
        
        List<string>.Enumerator e1 = writtenLines.GetEnumerator();
        while(e1.MoveNext())
        {
            contents = contents + (e1.Current) + "\n";
        }
        contents = contents.Substring(0, contents.Length - 1);  //Erase the last \n

        File.WriteAllText(path, contents);
    }
}
