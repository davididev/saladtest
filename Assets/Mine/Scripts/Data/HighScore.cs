﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScore
{
    public CSVReaderWriter readWrite;

    public SortedSet<HSEntry> entries = new SortedSet<HSEntry>();

    public class HSEntry
    {
        public string name;
        public int score;

        public HSEntry(string n, int s)
        {
            name = n;
            score = s;
        }

    }

    public class HighScoreComparer: IComparer<HSEntry>
    {
        public int Compare(HSEntry x, HSEntry y)
        {
            if (y.score == x.score)
                return 1;
            return y.score.CompareTo(x.score);
        }
    }


    public HighScore()
    {
        entries = new SortedSet<HSEntry>(new HighScoreComparer());
        readWrite = new CSVReaderWriter();
        LoadHighScore();
    }

    public void LoadHighScore()
    {
        Debug.Log("Loading file");
        string[] fileContents = readWrite.GetLines("hs.csv");
        if(fileContents == null)
        {
            Debug.Log("No file");
            //Create default file- we're gonna test the sort by putting in the default scores randomly
            //We're also going to test the clipping those outside the top 15
            entries.Add(new HSEntry("Dr Impossible", 5000));
            entries.Add(new HSEntry("Mr Loser", 1));
            entries.Add(new HSEntry("Greg", 4));
            entries.Add(new HSEntry("Sniffles", 2));
            entries.Add(new HSEntry("Gary", 3));
            entries.Add(new HSEntry("Barry", 12));
            entries.Add(new HSEntry("Larry", 9));
            entries.Add(new HSEntry("Mary", 12));
            entries.Add(new HSEntry("Fairly", 15));
            entries.Add(new HSEntry("John", 2));
            entries.Add(new HSEntry("Jodi", 8));
            entries.Add(new HSEntry("Kodi", 6));
            entries.Add(new HSEntry("Lara", 5));
            entries.Add(new HSEntry("Loren", 2));
            entries.Add(new HSEntry("Sniffles", 4));
            entries.Add(new HSEntry("Quackers", 7));
            entries.Add(new HSEntry("Crystals", 1));


            SaveHighScore();
        }
        else
        {
            entries.Clear();
            for (int i = 0; i < fileContents.Length; i++)
            {
                object[] rawObj = readWrite.GetParameters(fileContents[i]);
                string pname = (string) rawObj[0];
                int pscore = (int) rawObj[1];
                entries.Add(new HSEntry(pname, pscore));
            }
        }
    }

    public void SaveHighScore()
    {
        int top15Counter = 0;  //Only process the top 15 in entry
        
        readWrite.ResetWriteFile();

        SortedSet<HSEntry>.Enumerator e1 = entries.GetEnumerator();
        while (e1.MoveNext())
        {
            if(top15Counter < 15)
                readWrite.WriteLine(new object[] { e1.Current.name, e1.Current.score });
            top15Counter++;
        }

        readWrite.WriteFile("hs.csv");
    }
}
