﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CuttingBoard : MonoBehaviour
{
    public Image fillBar;
    public TMPro.TextMeshProUGUI currentIngredientList;
    private Salad salad = null;
    public int lastPlayerID = -1;
    public GameObject menuCanvas, saladImage;
    public Image arrowImage;
    public Sprite player1Image, player2Image;
    // Start is called before the first frame update
    void Start()
    {
        //This was to test the text bubble
        //GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "Hi, my name is", gameObject.name }, new int[] { -1, -1, -1 });
    }

    //Send Message that calls on all tags "LevelObj"
    void ResetFloor()
    {
        lastPlayerID = -1;
        salad = null;
    }

    /// <summary>
    /// Went a little away from the design doc here- going to have a generic "Interact" function for all raycasts from player
    /// </summary>
    /// <param name="p"></param>
    public void Interact(Player p)
    {
        if (p.playerID == 0)
            arrowImage.sprite = player1Image;
        if (p.playerID == 1)
            arrowImage.sprite = player2Image;
        p.frozen = true;

        if(p.heldSalad != null)
        {
            GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "I already have", "a salad!" }, new int[] { -1, -1, -1 });
            CloseUI(p);
            return;
        }

        if(p.cuttingBoardRef != null && p.cuttingBoardRef != this)  //Already bound to this cutting board
        {
            GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "You don't need to use", "two cutting boards!" }, new int[] { -1, -1, -1 });
            CloseUI(p);
            return;
        }

        if (lastPlayerID == -1)
        {
            lastPlayerID = p.playerID;
        }
        else
        {
            if (lastPlayerID != p.playerID)  //Another played tried to claim the cutting board- use the other one
            {
                GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] {"This one is", "already claimed."}, new int[] { -1, -1, -1});
                CloseUI(p);
                return;
            }
        }

        if (!(p.heldVeggies[0] == -1 && p.heldVeggies[1] == -1))  //Holding veggies- place em before you bring up the menu
        {
            if (salad == null)
                salad = new Salad();
            int x = 0;
            salad.AddIngredients(p.heldVeggies);
            p.heldVeggies[0] = -1;
            p.heldVeggies[1] = -1;
        }

        //We haven't even so much as dropped ingredients
        if(salad == null)
        {
            GlobalUI.GetInstance().CreateTextBubble(p.transform.position, new string[] { "I need to pick", "some vegetables."});
            p.frozen = false;
            saladImage.SetActive(false);
            menuCanvas.SetActive(false);
            return;
        }
        
        saladImage.SetActive(salad.IsReady());
        
        bool somethingToCut = false;
        
        string t = "Cut\n";
        for (int i = 0; i < salad.ingredients.Length; i++)
        {
            if (salad.ingredients[i] == -1 || salad.cut[i] == false)
            {
                somethingToCut = true;   
            }
            if(salad.ingredients[i] > -1)
                t = t + "<sprite=\"Vegetables\" index=" + salad.ingredients[i] + ">";

        }
        currentIngredientList.text = t;
        currentIngredientList.gameObject.SetActive(somethingToCut);

        menuCanvas.SetActive(true);
        StartCoroutine(AwaitInput(p));


    }

    void CloseUI(Player p)
    {
        p.frozen = false;
        menuCanvas.SetActive(false);
    }

    IEnumerator AwaitInput(Player p)
    {
        p.cuttingBoardRef = this;
        bool running = true;
        while(running)  //Make sure the game is active
        {
            if (PlayerCamera.RunCamera == false)
                break;

            Vector2 mv = new Vector2(Input.GetAxis("Horizontal" + p.playerID), Input.GetAxis("Vertical" + p.playerID)).normalized;
            if(Mathf.Approximately(mv.x, -1))  //Pressed left
            {
                bool somethingToCut = false;
                if(salad != null)
                { 
                    for(int i = 0; i < salad.ingredients.Length; i++)
                
                    if(salad.ingredients[i] != -1)
                    {
                        somethingToCut = true;
                    }    
                    else
                    {
                        if (salad.cut[i] == false)
                            somethingToCut = true;
                    }
                
                }
                
                if(somethingToCut)
                {
                    CutSalad(p);
                    while (currentCutting)
                    {
                        yield return new WaitForEndOfFrame();
                    }
                }
                    
            }
            if (Mathf.Approximately(mv.x, 1))  //Pressed right- retrieve salad
            {
                if (salad != null && salad.IsReady())
                {
                    p.heldSalad = GetSalad(p);

                    GlobalUI.GetInstance().CreateTextBubble(p.transform.position, new string[] { "Order up!" }, p.heldSalad.ingredients);
                    CloseUI(p);
                    running = false;
                }
                
            }
            if (Mathf.Approximately(mv.y, 1f))  //Cancel
            {
                CloseUI(p);
                running = false;
            }
                

            yield return new WaitForEndOfFrame();
            
        }
        
    }

    


    void CutSalad(Player p)
    {
        if (currentCutting)
            return;
        StartCoroutine(CutSaladRoutine(p));
    }

    bool currentCutting = false;
    IEnumerator CutSaladRoutine(Player p)
    {
        p.ccm.anim.SetBool("Chopping", true);
        currentCutting = true;
        //salad.AddIngredients(uncutVegetables);
        float maxTime = salad.GetTotalCutTime();
        float minTime = 0f;
        p.frozen = true;
        while(minTime < maxTime)
        {
            float perc = minTime / maxTime;
            //TODO: Fill the UI image here, currently non-existant
            fillBar.fillAmount = perc;
            fillBar.color = Color.Lerp(Color.red, Color.green, perc);
            //Adding to multipler if you have speed
            float mult = 1f;
            if (p.doubleSpeedTimer > 0f)
                mult = 2f;
            minTime += Time.deltaTime * mult;
            yield return new WaitForEndOfFrame();

        }

        


        currentCutting = false;
        fillBar.fillAmount = 0f;
        p.ccm.anim.SetBool("Chopping", false);
        CloseUI(p);

    }

    public Salad GetSalad(Player p)
    {
        p.cuttingBoardRef = null;  //We're done with the cutting board, release the reference so you can use the other one if you need to
        Salad newSalad = new Salad(salad);
        salad = null;
        return newSalad;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
