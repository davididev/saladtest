﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VegetablePickup : MonoBehaviour
{
    // Start is called before the first frame update
    public int vegetableID = 0;

    void Start()
    {
        
    }

    void Interact(Player p)
    {
        if (p.heldVeggies[0] == -1)
        {
            p.heldVeggies[0] = vegetableID;
            return;
        }
        if (p.heldVeggies[1] == -1)
        {
            p.heldVeggies[1] = vegetableID;
            return;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
