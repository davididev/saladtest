﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Mosty activates/deactivates Customers
/// </summary>
public class GameController : MonoBehaviour
{
    public static int Count = 0;
    private GameObject[] pool, pool2;
    public Transform root, root2;
    private Coroutine mainRoutine;
    private Player[] players;
    // Start is called before the first frame update
    void Start()
    {
        pool = new GameObject[root.childCount];
        for (int i = 0; i < pool.Length; i++)
        {
            pool[i] = root.GetChild(i).gameObject;
        }
        pool2 = new GameObject[root2.childCount];
        for (int i = 0; i < pool2.Length; i++)
        {
            pool2[i] = root2.GetChild(i).gameObject;
        }

        players = new Player[2];
        players[0] = GameObject.FindWithTag("Player1").GetComponent<Player>();
        players[1] = GameObject.FindWithTag("Player2").GetComponent<Player>();


    }

    /// <summary>
    /// Get instance from the game object tag.  Will stop working if we add another object with the GameController tag
    /// </summary>
    /// <returns></returns>
    public static GameController GetInstance()
    {
        return GameObject.FindWithTag("GameController").GetComponent<GameController>();
    }

    /// <summary>
    /// Player finished quickly- give powerup only to this specific player ID
    /// </summary>
    /// <param name="playerID">0 for player 1, 1 for player 2</param>
    public void SpawnPowerup(int playerID)
    {
        if(Powerup.CurrentCount <= pool2.Length)
        {
            bool success = false;
            while(success == false)
            {
                for(int i = 0; i < pool2.Length; i++)
                {
                    if(pool2[i].activeInHierarchy == false)
                    {
                        int rand = Random.Range(1, 2);
                        if(rand == 1)
                        {
                            pool2[i].SetActive(true);
                            success = true;
                            break;
                        }
                    }
                }
            }
            

        }
    }

    //Send Message that calls on all tags "LevelObj"
    void ResetFloor()
    {
        Count = 0;
        for(int i = 0; i < pool.Length; i++)
        {
            pool[i].SetActive(false);
        }
        for (int i = 0; i < pool2.Length; i++)
        {
            pool2[i].SetActive(false);
        }
        if (mainRoutine != null)
            StopCoroutine(mainRoutine);
        mainRoutine = StartCoroutine(CreateCustomers());
    }

    IEnumerator CreateCustomers()
    {
        yield return new WaitForSeconds(0.1f);
        while(gameObject)
        {

            bool slotTaken = false;
            if(Customer.Count != pool.Length)
            {
                while (!slotTaken)
                {
                    int rand = Random.Range(0, (pool.Length - 1));
                    if (pool[rand].activeInHierarchy == false)
                    {
                        pool[rand].SetActive(true);
                        slotTaken = true;
                    }
                }
            }
            
            
            yield return new WaitForSeconds(20f);
            while (Customer.Count == pool.Length)  //All slots taken
            {
                yield return new WaitForSeconds(30f);
            }
        }
    }

    
    // Update is called once per frame
    void Update()
    {
        if(PlayerCamera.RunCamera == true)  //Game currently running
        {
            int deadPlayers = 0;
            for(int i = 0; i < players.Length; i++)
            {
                if (players[i].TimeLeft <= 0)
                    deadPlayers++;
            }

            if(deadPlayers == 2)  //Both players are out
            {
                GlobalUI.GetInstance().SetPanel(2);  //Set to game over
            }
        }
    }
}
