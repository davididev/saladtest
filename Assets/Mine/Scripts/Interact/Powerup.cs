﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour
{
    public static int CurrentCount = 0;
    private int lockToPlayerID = 0;
    private const float ROTATE_SPEED = 25f;
    public enum MyType { Speed, Score, Time};
    public MyType myType;
    public Transform rotateThisObject;

    // Start is called before the first frame update
    void OnEnable()
    {
        CurrentCount++;
    }

    private void OnDisable()
    {
        CurrentCount--;
    }

    //Should be called by Send Message when spawned
    void WhichPlayer(int id)
    {
        lockToPlayerID = id;
    }

    // Update is called once per frame
    void Update()
    {
        rotateThisObject.Rotate(new Vector3(0f, ROTATE_SPEED * Time.deltaTime, 0f), Space.Self);
    }

    private void OnTriggerEnter(Collider other)
    {
        Player p = other.GetComponent<Player>();
        if(p != null)
        {
            if(p.playerID == lockToPlayerID)  //Only the player that unlocked this powerup can collect it
            {
                if (myType == MyType.Speed)
                {
                    p.doubleSpeedTimer = 30f;
                    GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "Speed" });
                }
                    
                if (myType == MyType.Score)
                {
                    p.Score += 40;
                    GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "Extra", "Points" });
                }
                    
                if (myType == MyType.Time)
                {
                    GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "Extra", "Time" });
                    p.TimeLeft = p.TimeLeft * 130 / 100;
                }
                    

                gameObject.SetActive(false);
            }
        }
    }
}
