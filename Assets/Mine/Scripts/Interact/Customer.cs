﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Customer : MonoBehaviour
{
    public static int Count = 0;
    private Salad requestedSalad;
    private float timeLeft = 0f, timeAtAnger = 0f, startingTime = 0f;
    private bool firstAngry = true;
    private bool[] playersFailed = { false, false };

    private Animator anim;
    // Start is called before the first frame update
    void OnEnable()
    {
        Count++;
        playersFailed = new bool[] { false, false };
        firstAngry = true;
        GameController.Count++;
        int count = 2;
        if (GameController.Count > 15)  //Let's up the difficulty by asking for three vegetables after the game has been running a while
            count = 3;

        startingTime = 25f * count;
        timeLeft = startingTime;
        timeAtAnger = 10f * count;

        requestedSalad = new Salad();
        int[] ing = new int[count];
        for(int i = 0; i < ing.Length; i++)
        {
            ing[i] = Random.Range(0, 5);
        }
        anim = GetComponent<Animator>();
        anim.SetBool("Chopping", false);  //Reset since we're not instantiating these
        requestedSalad.ingredients = ing;
        GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "Can I get" }, requestedSalad.ingredients);
    }

    private void OnDisable()
    {
        Count--;
    }

    //Called by player raycast
    void Interact(Player g)
    {
        if(g.heldSalad == null)
        {
            GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "Can I get" }, requestedSalad.ingredients);
            return;
        }



        if (g.heldSalad.Equals(requestedSalad))
        {
            float perc = timeLeft / startingTime;
            if (perc >= 0.7f)
            {
                GameController.GetInstance().SpawnPowerup(g.playerID);
            }
            if(timeLeft > timeAtAnger)  //Satisfied customer
            {
                g.Score += 3 * requestedSalad.ingredients.Length;
                
            }
            else  //Angry customer
                g.Score += 1 * requestedSalad.ingredients.Length;
            GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "Thanks!  I love it" });
            g.heldSalad = null;
            gameObject.SetActive(false);
        }
        else
        {
            playersFailed[g.playerID] = true;
            if(playersFailed[0] == true && playersFailed[1] == true)
            {
                GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "You both god it wrong.  Bye." });
                Player.GetPlayer(1).Score -= requestedSalad.ingredients.Length;
                Player.GetPlayer(2).Score -= requestedSalad.ingredients.Length;
                gameObject.SetActive(false);
                g.heldSalad = null;
                return;
            }
            else
            {
                GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "No!  I wanted" }, requestedSalad.ingredients);
                g.heldSalad = null;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (anim == null)
            anim = GetComponent<Animator>();
        if (PlayerCamera.RunCamera == false)  //Game not running
            return;
        timeLeft -= Time.deltaTime;
        if(timeLeft <= timeAtAnger && firstAngry)  //Only do it once
        {
            anim.SetBool("Chopping", true);  //We'll use the chopping animation for an angry customer
            firstAngry = false;
            GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "I still want" }, requestedSalad.ingredients);
        }

        if(timeLeft <= 0f)
        {
            GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "FINE GOODBYE." }, requestedSalad.ingredients);
            //Penalize both players
            Player.GetPlayer(1).Score -= requestedSalad.ingredients.Length;
            Player.GetPlayer(2).Score -= requestedSalad.ingredients.Length;
            gameObject.SetActive(false);
        }
    }
}
