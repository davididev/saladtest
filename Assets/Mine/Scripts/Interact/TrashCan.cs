﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashCan : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //This was to test the text bubbles
        //GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "Trashy"}, new int[] { 0, 2, 5 });
    }

    /// <summary>
    /// If we need this for anything, when the game restarts
    /// </summary>
    void ResetFloor()
    {

    }

    /// <summary>
    /// Message called by Player, this one will remove the contents of your 
    /// </summary>
    /// <param name="p"></param>
    void Interact(Player p)
    {
        if(!(p.heldVeggies[0] == -1 && p.heldVeggies[1] == -1))  //Currently holding something
            p.Score -= 1;

        p.heldVeggies = new int[] { -1, -1 };
        if(p.heldSalad != null)
            p.Score -= 3;
        p.heldSalad = null;
        
        GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "I threw away my", "veggies." });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
