﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextBubble : MonoBehaviour
{
    public const float PADDING_X = 11f;  //Sliced image width
    public const float PADDING_Y = 21f;  //Sliced image height
    public const float CHAR_WIDTH = 16f;  //Not sure on what it is yet
    public const float CHAR_HEIGHT = 16f;  //Not sure on what it is yet

    public TMPro.TextMeshProUGUI textObj;
    public AnimationClip mainSequenceAnim;  //Just to get the length of the anim; animation will be controlled by Animator
    public Animator anim;
    private RectTransform rect;

    // Start is called before the first frame update
    void OnEnable()
    {
        rect = GetComponent<RectTransform>();
    }

    /// <summary>
    /// Technically this triggers the text bubble, doesn't just set size.
    /// </summary>
    /// <param name="text">Lines of text</param>
    /// <param name="emoticans">Vegetable IDs that link to TMPro sprites</param>
    public void SetSize(string[] text, int[] emoticans)
    {
        int widthUnits = 3;  //Minimum for the 3 emoticans
        int heightUnits = text.Length + 1;
        string t = "";
        for(int i = 0; i < text.Length; i++)
        {
            if (text[i].Length > widthUnits)
                widthUnits = text[i].Length;

            t = t + text[i] + "\n";
        }
        //Print the emoticans here as the final line
        for(int z = 0; z < emoticans.Length; z++)
        {
            if(emoticans[z] > -1)
                t = t + "<sprite=\"Vegetables\" index=" + emoticans[z] + ">";
        }
        

        textObj.text = t;

        float x = (CHAR_WIDTH * widthUnits) + PADDING_X;
        float y = (CHAR_HEIGHT * heightUnits) + PADDING_Y;
        rect.sizeDelta = new Vector2(x, y);

        anim.SetTrigger("Activate");
        Invoke("DisableMe", mainSequenceAnim.length + 0.2f);  //Give it a little bit of time to transfer back to the Idle Animation before disabling it
    }

    void DisableMe()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
