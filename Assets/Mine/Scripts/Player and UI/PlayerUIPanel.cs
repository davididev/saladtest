﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUIPanel : MonoBehaviour
{
    public TMPro.TextMeshProUGUI mainText;
    public GameObject saladIndicator;
    private Player playerObj;
    public int ID = 1;


    // Start is called before the first frame update
    void OnEnable()
    {
        GameObject g = GameObject.FindWithTag("Player" + ID);
        playerObj = g.GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if(playerObj == null)
        {
            GameObject g = GameObject.FindWithTag("Player" + ID);
            playerObj = g.GetComponent<Player>();
        }

        int seconds = playerObj.TimeLeft;
        int minutes = 0;
        while(seconds >= 60)
        {
            minutes++;
            seconds -= 60;
        }
        string timeStr = minutes + ":" + seconds.ToString("D2");
        string str = playerObj.nameEntry + "\n" + timeStr + "\nScore: " + playerObj.Score.ToString("D3") + "\n";
        for (int z = 0; z < playerObj.heldVeggies.Length; z++)
        {
            if (playerObj.heldVeggies[z] > -1)
                str = str + "<sprite=\"Vegetables\" index=" + playerObj.heldVeggies[z] + ">";
        }

        mainText.text = str;
        saladIndicator.SetActive(playerObj.heldSalad != null);

    }
}
