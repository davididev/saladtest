﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalUI : MonoBehaviour
{
    public static HighScore HighScore;  //For some reaosn this didn't work as a singleton class
    //so I'm creating it as a singleton reference to GlobalUI
    public float SCREEN_WIDTH = 996f, SCREEN_HEIGHT = 441f;
    public GameObject instructionsPanel, gamePlayingPanel, gameOverPanel, pausePanel;
    [SerializeField] private TextBubble[] brQuadTextBubbles, blQuadTextBubbles, trQuadTextBubbles, tlQuadTextBubbles;
    public TMPro.TMP_InputField namePlayer1, namePlayer2;

    

    // Start is called before the first frame update
    void Start()
    {
        HighScore = new HighScore();
        c = GetComponent<Canvas>();
        SCREEN_WIDTH = c.pixelRect.width;
        SCREEN_HEIGHT = c.pixelRect.height;
        SetPanel(0);
    }


    private int currentPanel = 0;
    /// <summary>
    /// Set the global UI panel
    /// </summary>
    /// <param name="panelID">0 = instructions, 1 = game, 2 = game over, 3 = pause</param>
    public void SetPanel(int panelID)
    {
        currentPanel = panelID;
        instructionsPanel.SetActive(panelID == 0);
        if(panelID == 0)
        {
            namePlayer1.text = "David";
            namePlayer2.text = "Tim";
        }
        gamePlayingPanel.SetActive(panelID == 1);
        gameOverPanel.SetActive(panelID == 2);
        pausePanel.SetActive(panelID == 3);

        PlayerCamera.RunCamera = panelID == 1;  //Only move the camera if game is running / unpaused
    }

    /// <summary>
    /// Called by the instructions panel
    /// </summary>
    public void StartGame()
    {
        GameObject.FindWithTag("Player1").SendMessage("InitPlayer", namePlayer1.text);
        GameObject.FindWithTag("Player2").SendMessage("InitPlayer", namePlayer2.text);
        GameObject.FindWithTag("GameController").SendMessage("ResetFloor");
        GameObject[] interactObjs = GameObject.FindGameObjectsWithTag("LevelObj");
        foreach(GameObject g in interactObjs)
        {
            g.SendMessage("ResetFloor");
        }
        SetPanel(1);
    }

    

    public static GlobalUI GetInstance()
    {
        GameObject g = GameObject.FindWithTag("MainUI");
        return g.GetComponent<GlobalUI>();
    }

    /// <summary>
    /// Should be called by the object that wants to spawn the text bubble
    /// </summary>
    /// <param name="worldPos">Position to print it to</param>
    /// <param name="txt">Lines to put in the text box</param>
    public void CreateTextBubble(Vector3 worldPos, string[] txt)
    {
        int[] df = new int[] { -1, -1, -1 };
        CreateTextBubble(worldPos, txt, df);
    }

    /// <summary>
    /// Should be called by the object that wants to spawn the text bubble
    /// </summary>
    /// <param name="worldPos">Position to print it to</param>
    /// <param name="txt">Lines to put in the text box</param>
    /// <param name="emoticans">Vegetable ids to print</param>
    public void CreateTextBubble(Vector3 worldPos, string[] txt, int[] emoticans)
    {
        Vector3 uiPosition = Camera.main.WorldToViewportPoint(worldPos, Camera.MonoOrStereoscopicEye.Mono);
        if (uiPosition.x > 0.8f)
            uiPosition.x = 0.8f;
        if (uiPosition.y > 0.8f)
            uiPosition.y = 0.8f;
        if (uiPosition.x < 0.2f)
            uiPosition.x = 0.2f;
        if (uiPosition.y < 0.2f)
            uiPosition.y = 0.2f;
        Debug.Log("Viewport of text bubble: " + uiPosition);
        if(uiPosition.y >= 0.5f)  //Bottom
        {
            if(uiPosition.x >= 0.5f)  //Bottom left
            {
                DrawText(blQuadTextBubbles, txt, emoticans, uiPosition);
            }
            else //Bottom right
            {
                DrawText(brQuadTextBubbles, txt, emoticans, uiPosition);
            }
        }
        else
        {
            if (uiPosition.x >= 0.5f)  //Top left
            {
                DrawText(tlQuadTextBubbles, txt, emoticans, uiPosition);
            }
            else //Top right
            {
                DrawText(trQuadTextBubbles, txt, emoticans, uiPosition);
            }
        }
    }

    /// <summary>
    /// Internal function called from CreateTextBubble so we don't copy/paste several lines 4 times.
    /// Goes through specified array, finding an inactive one, and then drawing the text on it
    /// </summary>
    /// <param name="myArray">The array we're checking</param>
    /// <param name="txt">The text array to add to the text bubble.</param>
    /// <param name="emoticans">Vegetables to print on the text bubble</param>
    void DrawText(TextBubble[] myArray, string[] txt, int[] emoticans, Vector3 viewport)
    {
        for(int i = 0; i < myArray.Length; i++)
        {
            if(myArray[i].gameObject.activeInHierarchy == false)
            {
                myArray[i].gameObject.SetActive(true);
                myArray[i].SetSize(txt, emoticans);
                Vector3 v = new Vector3(viewport.x * SCREEN_WIDTH, (viewport.y * SCREEN_HEIGHT), 0f);
                v.x -= (SCREEN_WIDTH / 2f);
                v.y -= (SCREEN_HEIGHT / 2f);
                
                myArray[i].transform.localPosition = new Vector3(v.x, v.y, 0f);
                return;
            }
        }
    }

    Canvas c;
    // Update is called once per frame
    void Update()
    {
        if(c == null)  //Only retrieve the reference if it got lose
            c = GetComponent<Canvas>();

        SCREEN_WIDTH = c.pixelRect.width;
        SCREEN_HEIGHT = c.pixelRect.height;
        //In case the resolution changed

        if(Input.GetKeyUp(KeyCode.Escape))
        {
            if(currentPanel == 1)
            {
                SetPanel(3);  //Pause the game
                Time.timeScale = 0f;
                return;
            }
            if (currentPanel == 3)
            {
                SetPanel(1);  //UnPause the game
                Time.timeScale = 1f;
                return;
            }
        }
    }
}
