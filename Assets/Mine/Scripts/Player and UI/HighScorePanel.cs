﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScorePanel : MonoBehaviour
{
    // Start is called before the first frame update
    public TMPro.TextMeshProUGUI textLeft, textRight;
    void OnEnable()
    {
        GlobalUI.HighScore.LoadHighScore();
        for (int i = 1; i < 3; i++)
        {
            Player p = Player.GetPlayer(i);
            GlobalUI.HighScore.entries.Add(new HighScore.HSEntry(p.nameEntry, p.Score));
        }
        GlobalUI.HighScore.SaveHighScore();

        string leftStr = "High Scores: \n";
        string rightStr = "";
        int x = 0;
        SortedSet<HighScore.HSEntry>.Enumerator e1 = GlobalUI.HighScore.entries.GetEnumerator();
        while(e1.MoveNext())
        {
            if (x >= 0 && x < 7)
                leftStr += e1.Current.name + ": " + e1.Current.score.ToString("D4") + "\n";
            if (x >= 7 && x < 15)
                rightStr += e1.Current.name + ": " + e1.Current.score.ToString("D4") + "\n";
            x++;
        }
        textLeft.text = leftStr;
        textRight.text = rightStr;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
