﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    [HideInInspector] public int[] heldVeggies;
    private Vector3 startingPos;
    [HideInInspector] public CuttingBoard cuttingBoardRef = null;  //Just to make sure one player doesn't claim both cutting boards.
    [HideInInspector] public int Score = 0;
    [HideInInspector] public int TimeLeft = 0;
    [HideInInspector] public Salad heldSalad;
    [HideInInspector] public bool frozen = false;
    [HideInInspector] public int playerID = 0;
    public string nameEntry = "Davideyes";  //I plan to make a world canvas that prints the name on top of
                                            //the player, much like Super Smash Bros.
    [HideInInspector] public float doubleSpeedTimer = 0f;

    [HideInInspector] public CharacterControllerMomvement ccm;

    [Header("Scan Settings")]
    [SerializeField] protected BoxCollider scanCollider;
    [SerializeField] protected LayerMask interactMask;

    [Header("Player Label settings")]
    public TMPro.TextMeshProUGUI playerNameText;
    public Canvas playerWorldCanvas;

    private float startingSpeed = 0f;
    private bool firstDied = true;  //Toggle for the text bubble when you run out of time

    /// <summary>
    /// Get the player script from the tag name
    /// </summary>
    /// <param name="playerNum">Should be 1 or 2</param>
    /// <returns></returns>
    public static Player GetPlayer(int playerNum)
    {
        return GameObject.FindWithTag("Player" + playerNum).GetComponent<Player>();
    }

    private void Start()
    {
        ccm = GetComponent<CharacterControllerMomvement>();
        startingSpeed = ccm.moveSpeed;
        startingPos = transform.position;
    }

    // Should be called by the UI controller as a SendMessage when the game start.  Cutting board / Trash can use ResetFloor
    void InitPlayer(string playerName)
    {
        firstDied = true;
        cuttingBoardRef = null;  //Just to make sure one player doesn't claim both cutting boards.
        int playID = 0;
        if (gameObject.tag == "Player2")
            playID = 1;
        TimeLeft = 180;
        //TimeLeft = 45;  //Temporary to test the game over screen
        nameEntry = playerName;
        ccm.cc.enabled = false;
        transform.position = startingPos;
        ccm.cc.enabled = true;

        playerID = playID;
        heldVeggies = new int[3] { -1, -1, -1 };
        Score = 0;

        playerWorldCanvas.GetComponent<RectTransform>().sizeDelta = new Vector2(nameEntry.Length * 16f, 16f);
        playerNameText.text = nameEntry;
    }

    //Putting them all under one Send Message instead, to improve the code efficiency
    /*
    public void PlaceVegetables()
    {
        //TODO: Write code later, should do a raycast for the cutting board layer 
        //I'm thinking this function will prompt whether it cuts the vegetables, places them, or cancels using the arrow pads
    }
    */

    void GameOver()
    {
        TimeLeft = 0;  //Don't let it go to the negatives
        ccm.moveVec = Vector2.zero;
        if (firstDied == true)
        {
            firstDied = false;
            GlobalUI.GetInstance().CreateTextBubble(transform.position, new string[] { "I ran out", "of time." });
        }
    }

    private float gameSecondTimer = 0f;  //Timer to detect if a full second has passed
    // Update is called once per frame
    void Update()
    {
        

        if(PlayerCamera.RunCamera)
        {

            if (doubleSpeedTimer > 0f)
            {
                ccm.moveSpeed = startingSpeed * 1.5f;
                doubleSpeedTimer -= Time.deltaTime;
            }
            else
            {
                ccm.moveSpeed = startingSpeed * 1f;
            }
            gameSecondTimer += Time.deltaTime;
            if(gameSecondTimer > 1f)
            {
                TimeLeft--;
                if (TimeLeft <= 0)
                    GameOver();
                gameSecondTimer -= 1f;
            }
            playerWorldCanvas.transform.position = transform.position + (Vector3.up * 3f);
            if (ccm == null)
                ccm = GetComponent<CharacterControllerMomvement>();

            ccm.moveVec = new Vector2(Input.GetAxis("Horizontal" + playerID), Input.GetAxis("Vertical" + playerID)).normalized;
            if (frozen)
                ccm.moveVec = Vector2.zero;

            if(Input.GetButtonDown("Interact" + playerID) == true)
            {

                Collider[] cs = PhysicsExtensions.OverlapBox(scanCollider, interactMask);
                foreach(Collider c in cs)
                {
                    c.gameObject.SendMessage("Interact", this, SendMessageOptions.DontRequireReceiver);
                }
            }
        }
        
    }
}
