﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public const float MIN_HEIGHT = 4f;
    public const float MAX_HEIGHT = 25f;
    public const float MAX_DIST = 24f;

    public static bool RunCamera = false;
    private GameObject player1, player2;
    // Start is called before the first frame update
    void Start()
    {
        GetReferences();
    }

    /// <summary>
    /// Get the player references from tags
    /// </summary>
    void GetReferences()
    {
        if (player1 == null)
            player1 = GameObject.FindGameObjectWithTag("Player1");
        if (player2 == null)
            player2 = GameObject.FindGameObjectWithTag("Player2");
    }

    // Update is called once per frame
    void Update()
    {
        GetReferences();

        Vector3 pt1 = player1.transform.position;
        Vector3 pt2 = player2.transform.position;
        float dist = Vector3.Distance(pt1, pt2);
        Vector3 centerPos = (pt1 + pt2) / 2f;


        float perc = dist / MAX_DIST;
        float h = (MAX_HEIGHT - MIN_HEIGHT) * perc;

        transform.position = centerPos + (Vector3.up * (h + MIN_HEIGHT));
    }
}
