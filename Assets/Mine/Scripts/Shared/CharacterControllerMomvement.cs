﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public class CharacterControllerMomvement : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector2 moveVec;
    public float moveSpeed = 8f;
    private const float ROTATE_PER_SECOND = 360f;
    [HideInInspector] public CharacterController cc;
    [HideInInspector] public Animator anim;

    private float targetRot = 180f;

    void Start()
    {
        GetReferences();
    }

    void GetReferences()
    {
        if(cc == null)
            cc = GetComponent<CharacterController>();
        if(anim == null)
            anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        GetReferences();
        cc.SimpleMove(new Vector3(moveVec.x, 0f, moveVec.y) * moveSpeed);  //No need to make it comlpicated for such a simple game
        if(moveVec != Vector2.zero)
            targetRot = (Mathf.Atan2(-moveVec.y, moveVec.x) * Mathf.Rad2Deg) + 90f;

        Vector3 rot = transform.eulerAngles;
        rot.y = Mathf.MoveTowardsAngle(rot.y, targetRot, ROTATE_PER_SECOND * Time.deltaTime);
        transform.eulerAngles = rot;

        anim.SetBool("Walking", moveVec != Vector2.zero);
    }
}
